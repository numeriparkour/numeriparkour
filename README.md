# Numeriparkour

**Numéri Parkour** est un jeu de plateau, *serious game* (jeu sérieux); qui a pour vocation de faire découvrir les difficultés rencontrer par des utilisateur⋅ice⋅s lors de parcours *dématerialisés* tel que la recherche d'emplois ou du RSA. C'est un jeu libre qui a pour objectif la sensibilisation des publics en situation de confort d'usages et de matériels numériques.